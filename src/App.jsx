import NoControlado from "./components/NoControlado";

const App = () => {
  return (
    <div className="container">
      <h1>Formularios</h1>
      <NoControlado />
    </div>
  );
};

export default App;
