import { useRef } from "react";

const NoControlado = () => {
  const form = useRef(null);

  const handleSubmit = (e) => {
    e.preventDefault();
    const data = new FormData(form.current);

    const { title, description, state } = Object.fromEntries(data.entries());

    // validar los datos
    if (title.trim() === "") {
      return console.error("Titulo vacio");
    }

    if (description.trim() === "") {
      return console.error("Descripción vacia");
    }

    if (state.trim() === "") {
      return console.error("Estado vacio");
    }
  };

  return (
    <>
      <form onSubmit={handleSubmit} ref={form}>
        <input
          type="text"
          placeholder="Ingrese Todo"
          className="form-control mb-2"
          name="title"
          defaultValue="Tdo 1"
        />
        <textarea
          className="form-control mb-2"
          placeholder="Ingrese descripción"
          name="description"
          defaultValue="Descripción 1"
        ></textarea>
        <select
          className="form-select mb-2"
          name="state"
          defaultValue="Pendiente"
        >
          <option value="Pendiente">Pendiente</option>
          <option value="Completado">Completado</option>
        </select>
        <button className="btn btn-primary" type="submit">
          Procesar
        </button>
      </form>
    </>
  );
};

export default NoControlado;
